// https://stackoverflow.com/questions/26423537/how-to-position-the-input-text-cursor-in-c

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define clear() printf("\033[H\033[J")
#define gotoxy(x,y) printf("\033[%d;%dH", (y), (x))

// ********************************

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h> // for nanosleep
#else
#include <unistd.h> // for usleep
#endif

void sleep_ms(int milliseconds)
{ // cross-platform sleep function
#ifdef WIN32
   Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
   struct timespec ts;
   ts.tv_sec = milliseconds / 1000;
   ts.tv_nsec = (milliseconds % 1000) * 1000000;
   nanosleep(&ts, NULL);
#else
   if (milliseconds >= 1000)
      sleep(milliseconds / 1000);
   usleep((milliseconds % 1000) * 1000);
#endif
}

// *************************

int main(void)
{
    int number;

    clear();
    printf(
        "Enter your number in the box below\n"
        "+-----------------+\n"
        "|                 |\n"
        "+-----------------+\n"
    );
    gotoxy(2, 3);
    scanf("%d", &number);

    int n;
    int x;
    int y;

    // semente de geração de números aleatórios
    srand(time(NULL));

    for (int i = 0; i < 200; i++)
    {
        n = rand();
        x = (n % 80);
        n = rand();
        y = (n % 25);

        gotoxy(x,y);    
        sleep_ms(50);
        printf("*");
        fflush(stdout);
    }

    gotoxy(1,4);
    printf("\n");
    return 0;
}