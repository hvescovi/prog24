#include <stdio.h>
#include <string.h>


// return the position of the delimiter in the string
// update the "ret" with the substring
int my_get_part(char string2[], char delimiter[], int n_part, char *ret) {

    //printf("recebi: %s\n", string2);

    // storing in another variable, in order to make it works!
    // something about type conversion    
    char string[100];
    strcpy(string, string2);

    // pointer to found the delimiter
    char *pv;

    // find the first occurrency
    pv = strstr(string, delimiter);
    if (pv != NULL)
    {
        //get the part position
        long int pos = pv-string;

        //printf("Posição da parte 1: %ld\n", pos);
        
        //temporary variable
        char temp[100] = "";

        // get the part in the temp, quantity=pos
        memcpy( temp, &string, pos);
        // find the insere final de string
        temp[pos] = '\0';

        //printf("parte: %s", temp);

        strcpy( ret, temp);

        return pos;
    } else {
        // put all the string in the ret
        strcpy( ret, string);
        return 0;
    }
}

/*
const char * my_get_part(char string[], char delimiter[], int n_part) {
    return "oi";
}
*/

void main()
{
    char frase[100] = "Mary,ma@gmail.com,47922334455";
    char vem[100];

    int pos = 1; 
    while (pos > 0) {
        pos = my_get_part(frase, ",", 1, vem);
        printf("===> %s em %d\n", vem, pos);

        // size of the nem string
        int ns = sizeof(frase) - pos;
        //update the string
        for (int i=0; i< ns; i++) {
            frase[i] = frase[i+pos+1];
        }
        //memcpy( frase, &frase+pos);
        printf("NOVA frase: %s\n", frase);
    }
}