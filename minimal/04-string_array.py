# https://www.w3schools.com/python/python_arrays.asp

# python não suporta vetores, mas listas podem ser usadas

# criar uma lista vazia
lista = []

# adicionar
lista.append("Maria")
lista.append("João")

# mostrar
print("lista:")
for nome in lista:
    print(nome)

# modificar
lista[0] = "Tiago"

# exibir de novo
print("nome modificado:")
for nome in lista:
    print(nome)