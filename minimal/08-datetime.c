#include <stdio.h>
#include <time.h>

// ********************************

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h> // for nanosleep
#else
#include <unistd.h> // for usleep
#endif

void sleep_ms(int milliseconds)
{ // cross-platform sleep function
#ifdef WIN32
   Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
   struct timespec ts;
   ts.tv_sec = milliseconds / 1000;
   ts.tv_nsec = (milliseconds % 1000) * 1000000;
   nanosleep(&ts, NULL);
#else
   if (milliseconds >= 1000)
      sleep(milliseconds / 1000);
   usleep((milliseconds % 1000) * 1000);
#endif
}

// **************************************

int main()
{
   // variáveis de tempo
   time_t start_t, end_t;

   // diferença
   double diff_t;

   // quantas vezes fez o loop?
   int vezes = 0;

   // captura o tempo atual
   time(&start_t);

   // tenta 100 vezes
   while (vezes < 100)
   {
      // espera 500 milisegundos
      sleep_ms(500);

      printf(" ************** \n");

      // pega o momento atual de novo
      time(&end_t);

      // calcula a diferença
      diff_t = difftime(end_t, start_t);

      // registra o loop
      vezes++;

      // se já passou de 8 segundos
      if (diff_t > 8) {
         // sai do while
         break;
      }

      printf("=========================> %f ", diff_t);
   }
   return(0);
}