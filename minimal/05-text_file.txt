// programa que cadastra nome, email e telefone
// os dados ficam em arquivo texto, separados por vírgula (comma-separated values - CSV)

// inicializar a estrutura de dados que vai armazenar o cadastro

// se existir o arquivo de dados
    // abrir o arquivo, ler os dados para a estrutura, fechar o arquivo

// sinalizar que a interação com o usuários será repetida (continua = true)

//  enquanto interagir com o usuário (enquanto continua)

    // exibe o menu

    // pergunta o que o usuário deseja fazer

    // se for opção 1
        // listar o arquivo

    // se for opção 2
        // pedir novos nome, email e telefone
        // inserir os novos dados na estrutura

    // se for opção nove
        // sinalizar que não deseja mais executar a interação (continua = false)

// atualizar (sobrepor) o arquivo com a nova estrutura

// encerrar o programa