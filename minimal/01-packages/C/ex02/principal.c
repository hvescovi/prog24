#include <stdio.h>
#include "libs/mysplit.h"
#include "libs/mymath.h"

int main() {
    // using math function
    int a = 10;
    int b = 20;
    int x = somar_inteiros(a, b);
    printf("Soma: %d\n", x);

    // using split function
    char frase[100] = "Mary,ma@gmail.com,47922334455,Brazil";
    int n = count_parts(frase, ",");
    char parte[100];
    get_the_part(frase, ",", 2, parte);
    printf("Segunda parte: %s\n", parte);

    return 0;
}

/*

Executing:

$ gcc biblioteca.c principal.c -o principal
$ ./principal

*/