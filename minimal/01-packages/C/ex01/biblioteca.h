#ifndef BIBLIOTECA_H
#define BIBLIOTECA_H
// cabeçalhos
int somar_inteiros(int a, int b);
#endif

/*
O uso das macros #ifndef, #define e #endif serve para prevenir 
múltiplas inclusões acidentais (conhecido como include guard).
*/