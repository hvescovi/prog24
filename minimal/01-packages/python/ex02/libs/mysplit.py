def count_parts(frase, delim):
    tmp = frase.split(delim)
    return len(tmp)

def get_part(frase, delim, i):
    tmp = frase.split(delim)
    return tmp[i]

if __name__ == "__main__":
    # library test
    texto = "Mary,ma@gmail.com,47922334455,Brazil"
    n = count_parts(texto, ",")
    p = get_part(texto, ",", 1)
    print(p)