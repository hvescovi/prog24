from libs.mymath import *
from libs.mysplit import *

a = 10
b = 20
x = somar_inteiros(a,b)
print(f'Soma: {x}')

texto = "Mary,ma@gmail.com,47922334455,Brazil"
n = count_parts(texto, ",")
p = get_part(texto, ",", 1)
print(p)