#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void main()
{

    int final;
    int n;

    // semente de geração de números aleatórios
    srand(time(NULL));

    for (int i = 0; i < 20; i++)
    {
        // escolhe um número aleatório
        n = rand();
        // obtém o resto para delimitar uma faixa, por exemplo, entre 0 e 10
        final = (n % 11);

        printf("sorteado: %d\n", final);
    }
}