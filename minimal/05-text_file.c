#include <stdio.h>
#include <stdbool.h>
#include <string.h>

// programa que cadastra nome, email e telefone
// os dados ficam em arquivo texto, separados por vírgula (comma-separated values - CSV)

int main()
{
    // inicializar o vetor para armazenar as informações
    char vetor[100][250]; // até 100 elementos, cada um com até 250 caracteres
    
    // variável temporária para ler cada linha
    char buffer[250];

    // quantidade de elementos no vetor é zero
    int q = 0;

    // inicializar posição no vetor
    int i = 0;

    // se existir o arquivo de dados
    FILE *fp;
    fp = fopen("05-dados.csv", "r");
    if (fp != NULL)
    {
        // ler o arquivo linha por linha
        while (fgets(buffer, 250, fp))
        {
            printf("%s", buffer);

            // guardar a linha no vetor
            strcpy(vetor[i], buffer);

            // incrementa contador do vetor
            i++;
        }

        // atualizar o tamanho usado
        q = i;

        // fechar o arquivo
        fclose(fp);
    }
    else
    {
        printf("O arquivo de dados ainda não existe, nenhum dado foi lido\n");
    }

    // sinalizar que a interação com o usuários será repetida
    bool repete = true;

    // variável para armazenar opção do usuário
    int op;

    // variáveis para novo nome, email e telefone
    char nome[100];
    char email[100];
    char telefone[100];
    char novalinha[250];

    //  enquanto interagir com o usuário
    while (repete)
    {
        // exibe o menu
        printf("Menu:\n");
        printf("1 - listar dados\n");
        printf("2 - incluir dados\n");
        printf("9 - sair do programa\n");
        printf("Opção: ");

        // pergunta o que o usuário deseja fazer
        scanf("%d", &op);

        printf("Você escolheu: %d\n", op);

        printf("atualmente o arquivo possui %d linha(s):\n", q);

        // se for opção 1
        if (op == 1)
        {
            // listar o vetor
            for (int i = 0; i < q; i++)
            {
                // a linha do vetor já possui quebra de linha
                printf("%d: %s", i, vetor[i]);
            }
            //uma linha nova para não misturar com o menu
            printf("\n");
        }
        else if (op == 2)
        {
            // se for opção 2
            // pedir novos nome, email e telefone
            printf("Nome: ");
            scanf("%s", nome);
            printf("Email: ");
            scanf("%s", email);
            printf("Telefone: ");
            scanf("%s", telefone);

            // preparar a nova linha
            strcpy(novalinha, nome);
            strcat(novalinha, ",");
            strcat(novalinha, email);
            strcat(novalinha, ",");
            strcat(novalinha, telefone);

            printf("vou add: %s", novalinha);
            // adicionar no vetor
            strcpy(vetor[q], novalinha);
            // aumentar quantidade no controle
            q++;
        }
        else if (op == 9)
        {
            // se for opção zero
            // sinalizar que não deseja mais executar a interação
            repete = false;
        }
    } // fim while

    // TODO: atualizar (sobrepor) o arquivo com a nova estrutura

    // encerrar o programa
    return 0;
}