linha = '1999,"Acre","Janeiro",9,1999-01-01'
# quebrar a string usando vírgula
partes = linha.split(",")
# mostrar as partes
for p in partes: # for each
    print(p)
# mostrar o ano e a quantidade de incêndios
print(f'Ano: {partes[0]}, incêndios: {partes[3]}')