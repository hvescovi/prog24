#include <stdio.h>
#include <string.h>

void main()
{
    char string[100] = "Mary,ma@gmail.com,47922334455";
    char *pv;
    // encontrar a posição onde está a primeira vírgula
    pv = strstr(string, ",");
    if (pv != NULL)
    {
        //obtendo a parte 1
        long int pos = pv-string;
        printf("Posição da parte 1: %ld\n", pos);
        char temp[100];
        // copia em temp a string, qdt=pos
        memcpy( temp, &string, pos );
        // insere final de string
        temp[pos] = '\0';

        printf("Parte 1: %s\n", temp);

        // obtendo a parte 2
        // a nova string vai ser da vírgula em diante
        //char string2[100];
        memcpy( string, &string[pos+1],100 );
        //printf("nova string: %s\n", string);
        //char *pv2;
        pv = strstr(string, ",");
        if (pv != NULL)
        {
            pos = pv-string;
            memcpy( temp, &string, pos );
            // insere final de string
            temp[pos] = '\0';

            printf("Parte 2: %s\n", temp);

            // obtendo a parte 3
            // a nova string vai ser da vírgula em diante
            //char string2[100];
            memcpy( string, &string[pos+1],100 );
            //printf("nova string: %s\n", string);
            //char *pv2;
            pv = strstr(string, ",");
            if (pv != NULL)
            {
                pos = pv-string;
                memcpy( temp, &string, pos );
                // insere final de string
                temp[pos] = '\0';

                printf("Parte 3: %s\n", temp);
            } else {
                //pega o resto da string
                memcpy( temp, &string[0], 100 );
                printf("Parte 3: %s\n", temp);
            }
        }
    }
}