# https://www.pythontutorial.net/python-basics/python-write-text-file/
lines = ['1', '2', '3', 'oito', 'casa']

n = 1
ultimo = len(lines)

with open('10-dados.txt', 'w') as f:
    for line in lines:
        f.write(line)
        n+=1
        # só escreve nova linha se não passou do último
        if n <= ultimo:
            f.write('\n')