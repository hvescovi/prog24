# https://www.askpython.com/python/examples/difference-between-two-dates#:~:text=The%20datetime()%20constructor%20in,24%20hours%20as%20one%20day.

from datetime import date, datetime

data1 = date(2024, 2, 20)
data2 = date(2024, 3, 15)
dias = data2 - data1
print(f'Entre {data1} e {data2} há: {dias}')

s1 = '2024/2/20 14:00:00.0'
s2 = '2024/3/15 20:00:00.0'

data1 = datetime.strptime(s1, "%Y/%m/%d %H:%M:%S.%f")
data2 = datetime.strptime(s2, "%Y/%m/%d %H:%M:%S.%f")
dias = data2 - data1
print(f'Entre {data1} e {data2} há: {dias}')