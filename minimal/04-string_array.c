#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int main()
{
    // https://www.javatpoint.com/an-array-of-strings-in-c
    // https://www.tutorialspoint.com/cprogramming/c_array_of_strings.htm
    // https://www.scaler.com/topics/array-of-strings-in-c/


    // vetores podem ser matrizes de caracteres
    int linhas = 10;
    int colunas = 100;
    
    char vetor[linhas][colunas];
    strcpy(vetor[0],"Esta é a primeira frase");
    strcpy(vetor[1],"A segunda frase aqui");  
    
    // apenas 2 posiçõe ocupadas
    int tam = 2;

    printf("- Vetor:\n");
    for (int i=0; i < tam; i++){
        printf("%s\n", vetor[i]);
    }

    // alterando a segunda frase
    strcpy(vetor[1],"A segunda frase aqui MUDOU");  
    printf("- Vetor com frase modificada:\n");
    for (int i=0; i < tam; i++){
        printf("%s\n", vetor[i]);
    }

    // ocupando a terceira posição
    strcpy(vetor[2],"Terceira frase :-)");  
    tam++;
    printf("- Vetor com nova frase:\n");
    for (int i=0; i < tam; i++){
        printf("%s\n", vetor[i]);
    }
    
    // encerrar o programa
    return 0;
}