
program Hello;
var
  resultado : integer;

// passagem de parâmetros por valor
function somar(a, b : integer) : integer;
begin
  somar := a + b;
end;

// passagem de parâmetros por valor (variáveis "a" e "b") 
// e referência (variável "total")
procedure somar2(a, b : integer; var total: integer);
begin
  total := a + b;
end;

begin
  writeln ('Hello, World!');
  writeln (somar(5,7));
  somar2(10, 30, resultado);
  writeln(resultado);
end.