#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    char s[100];
    char saux[5];

    // define valor inicial da string
    strcpy(s,",1");

    // concatena!
    strcat(s, ",2");
    strcat(s, ",");

    // converte 18 para string, coloca em saux
    sprintf(saux,"%d",18);

    // concatena!
    strcat(s, saux);
    
    // mostra :-)
    printf("%s\n", s);
    
    return 0;
}
