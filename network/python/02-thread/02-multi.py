import threading
import time
import random

def func(name):
    print("func", name, 'rodando')
    d = 1 + random.randint(0,5)
    time.sleep(d)
    print("func", name, 'finished')

threads = list()

for i in range(3):
    x = threading.Thread(target=func, args=(i,))
    threads.append(x)
    x.start()

for t in threads:
    t.join()

print("END!")