import sys
import time
import random
from colorama import init as colorama_init
from colorama import Fore

colorama_init()

def print_there(x, y, text):
     sys.stdout.write("\x1b7\x1b[%d;%df%s\x1b8" % (x, y, text))
     sys.stdout.flush()

# clear area
for col in range(1,60):
    for lin in range(1,5):
        print_there(lin, col, "*")
        time.sleep(0.01)
        print_there(lin, col, " ")
        time.sleep(0.01)
       
# random prints
for x in range(1,30):
    x = random.randrange(1, 100)
    y = random.randrange(1, 30)
    print_there(y, x, f"{Fore.GREEN}HELLO")
    time.sleep(0.3)
    #print_there(y, x, "     ")
    