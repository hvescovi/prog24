package interfaces;

// https://medium.com/@jonesroberto/desing-patterns-factory-method-a7496ae071aa

public interface IFactory {
    void Drive(int miles);    
}
