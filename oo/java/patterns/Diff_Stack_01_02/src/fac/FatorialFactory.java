package fac;

public abstract class FatorialFactory {
    
    public abstract long fatorial(int n);
    
}
